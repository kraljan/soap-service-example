﻿using System.Runtime.Serialization;

namespace WebApi.Models;

//[DataContract]
[System.Runtime.Serialization.DataContractAttribute(Name = "Run", Namespace = "http://schemas.datacontract.org/2004/07/Contracts")]
public class SoapModel : ResponseBase
{
    private byte[] DataField;
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Name { get; set; }
    [System.Runtime.Serialization.DataMemberAttribute()]
    public byte[] Data
    {
        get
        {
            return this.DataField;
        }
        set
        {
            this.DataField = value;
        }
    }

}
[System.Runtime.Serialization.DataContractAttribute( Namespace = "http://schemas.datacontract.org/2004/07/Contracts")]

public partial class ResponseBase : object
{

    private string ErrorField;

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Error
    {
        get
        {
            return this.ErrorField;
        }
        set
        {
            this.ErrorField = value;
        }
    }
}
