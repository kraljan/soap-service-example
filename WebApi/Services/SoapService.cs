﻿using WebApi.Models;

namespace WebApi.Services;

public class SoapService : ISoapService
{
    public SoapModel Run(SoapModel model)
    {
        var data = new SoapModel
        {
            Name = $"Hello Karle",
            Data = new byte[] { 1, 2, 3, 4, 5 }
        };
        return data;
    }
}