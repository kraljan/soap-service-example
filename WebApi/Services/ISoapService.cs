﻿using System.ServiceModel;
using WebApi.Models;

namespace WebApi.Services;

[ServiceContract]
public interface ISoapService
{
    [OperationContract]
    SoapModel Run(SoapModel model);
}