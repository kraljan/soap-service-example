Projekt k otestovani funkcnosti serializace a deserializace SOAP zprav.

Priklad SOAP zpravy k poslani POSTem na https://localhost:7228/SoapService.svc :


<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:tem="http://tempuri.org/" >
	<soapenv:Header/>
	 <soapenv:Body>
		 <tem:Run>
		 </tem:Run>
	 </soapenv:Body>
</soapenv:Envelope>

Vysledek:
<s:Envelope
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<s:Body>
		<RunResponse
			xmlns="http://tempuri.org/">
			<RunResult
				xmlns:d4p1="http://schemas.datacontract.org/2004/07/Contracts"
				xmlns:i="http://www.w3.org/2001/XMLSchema-instance" i:type="d4p1:Run">
				<d4p1:Error i:nil="true" />
				<d4p1:Data>AQIDBAU=</d4p1:Data>
				<d4p1:Name>Hello Karle</d4p1:Name>
			</RunResult>
		</RunResponse>
	</s:Body>
</s:Envelope>


Problemem je uvedeny typ pro RunResult 'i:type="d4p1:Run"'.
Tento se vyskytne pouze v pripade, ze je datovy typ odvozeny z jineho datov�ho typu. Viz "public class SoapModel : ResponseBase"
V p��pad� vypusteni deden� se typ ve vyslednem XML nevyskytne."

RESENI: je potreba u derived class odstrani Name v konstruktoru "DataContractAttribute", pri jeho uplnem odstraneni by se nedostaly elementy z base tridy do vysledneho XML
a soucasne zde odstranit "KnownTypeAttribute"
